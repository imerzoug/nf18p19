#!/usr/bin/python3

import os
import psycopg2

#Greeter is a terminal application that greets old friends warmly,
#and remembers new friends.

############# VARIABLES GLOBALES ################
#Informations de connexion
HOST = "localhost"
USER = "postgres"
PASSWORD = "mdp"
DATABASE = "bibli"

#Se connecter a la bdd
dbConnection = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
#Creer un curseur pour executer des requetes
dbCursor = dbConnection.cursor()


#############FONCTIONS D'ACCES A LA BDD################
def DB_close_connection():
    dbCursor.close()
    dbConnection.close()


def DB_simple_search(): 
    critere=input("\n Saisissez le critere de recherche (parmis titre, dateSortie, langue, etatdudocumennt) : ")
    elt_recherche = input("\n Saisissez votre recherche : " )
    requete = "SELECT code, " + critere + " FROM document WHERE " + critere + "= '%s'" % elt_recherche
    parametres = (critere) 
    dbCursor.execute(requete,parametres)
    resultat = dbCursor.fetchone() 
    if(resultat) :
        d = 1
        print( "\n Resultats :\n")
	print(" \n " + critere )
        while (resultat):
            print(" " + str(d) + " -" + resultat[0] +" "+ resultat[1] + "\n \n \n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")

               

def DB_comptage():
    nombre = input("\n Saisissez le nombre minimum d'emprunt " )
    requete = "select d.titre, count(e.code) from EMPRUNTDOCUMENT as e, document as d where e.code = d.code group by d.titre having count(e.code) >= " + nombre
    parametres = (nombre)
    dbCursor.execute(requete,parametres)
    resultat = dbCursor.fetchone() 
    if(resultat) :
        d = 1
        print( "\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\tTitre : " + resultat[0])
            print("\tEmprunte : " + str(resultat[1]) + " fois \n\n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")


def DB_ChercherContri():
    titre = input("\n Saisissez le titre " )
    requete = "SELECT c.nom,c.prenom,c.datedenaissance,c.code, d.titre from CONTRIBUTIONDOCUMENT as c, document as d WHERE d.titre = '%s' and c.code = d.code " % titre
    parametres = (titre) 
    dbCursor.execute(requete,parametres)
    resultat = dbCursor.fetchone() 
    if(resultat) :
        d = 1
        print( "\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\tTitre et code : " "'" + resultat[4] + "'" " " +resultat[3])
            print("\tContributeur : " + resultat[0] +" "+ resultat[1] + " ne le " + str(resultat[2]) + " \n")
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")

def DB_EmpruntsAdherent():
    idcarte = input("\n Indiquez l'id de l'adherent: " )
    requete = "SELECT code FROM EMPRUNTDOCUMENT WHERE idcarte = '%s'" %idcarte
    parametres = (idcarte) 
    dbCursor.execute(requete,parametres)
    resultat = dbCursor.fetchone() 
    if(resultat) :
        d = 1
        print( "\n Resultats :\n")
        while (resultat):
            print(" " + str(d) + " -\t Adherent: " + idcarte)
            print("\t Emprunt du document: " + resultat[0])
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")

def DB_HistoriqueEmprunt():
    code = input("\n Indiquez le code du document " )
    requete = "SELECT idcarte, dateemprunt FROM EMPRUNTDOCUMENT WHERE code = '%s' ORDER BY dateemprunt DESC" % code
    parametres = (code)
    dbCursor.execute(requete,parametres)
    resultat = dbCursor.fetchone() 
    if(resultat) :
        d = 1
        print( "\n Resultats :\n")
	print("\n Le document %s a etait emprunte par: \n" %code)
        while (resultat):
            print(" " + str(d) + " -\t Adherent: " + resultat[0])
            print("\t le " + str(resultat[1]))
            resultat = dbCursor.fetchone()
            d = d+1
    else :
        print(" Aucun resultat ne correspond a votre recherche !\n")
   

############# FUNCTIONS : Affichage du menu ################

def display_title_bar():
    # Efface le contenu de l'ecran et affiche une banniere
    os.system('clear')
    print("\t\t**********************************************")
    print("\t\t*        Projet NF18 - POC Python            *")
    print("\t\t**********************************************")
    print("\n\n")

# Fonction pour l'affichage du menu de selection
# Retourne :la valeur saisie par l'utilisateur
def get_user_choice():
   
    print(" [1] Recherche simple de document par critere.")
    print(" [2] Nombre de document emprunte plus de X fois.") 
    print(" [3] Tout les contributeurs des documents ayant le titre T")
    print(" [4] Tout les emprunts d'un adherent")
    print(" [5] Historique de tout les adherents ayant emprunter un document D")
    # Rajouter le reste des lignes du menu ici
   
    print(" [q] Quit.")
    print("\n")
    return input(" Saisisez votre choix : ")


### MAIN ###
def main():
    # Boucle principale
    choice = ''
    while choice != 'q':
        # Recuperer le choix utlisateur
	os.system('clear')
        display_title_bar()   
        choice = get_user_choice()
        # Executer la foncion demandee
        if choice == '1':
            DB_simple_search()
        elif choice == '2':
            DB_comptage()
        elif choice == '3':
	    DB_ChercherContri()
	elif choice == '4':
	    DB_EmpruntsAdherent()
	elif choice == '5':
	    DB_HistoriqueEmprunt()
        elif choice == 'q':
            DB_close_connection()
            os.system('clear')
            return 0;

	choice = get_user_choice()
	os.system('clear')


if __name__ == "__main__":
   main()